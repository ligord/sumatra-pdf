# Sumatra pdf reader node wrapper

## 安装方法

   npm install git+http://dev.ligotop.com/zhangheng/sumatra-pdf.git

### 安装非 master 分支
   npm install git+http://dev.ligotop.com/zhangheng/sumatra-pdf.git#fix-memory
   在 package.json 里
   ```javascript
    "dependencies": {
      ...
      "sumatra-pdf": "git+http://dev.ligotop.com/zhangheng/sumatra-pdf.git#fix-memory",
      ...
    },
   ```

## 使用方法

```javascript
var Reader = require('sumatra-pdf');
var pdf = 'filepath';

//options = {pdf: 'path', restrict: false, ... }
var reader = new Reader(options);

// 查看模式，禁止下载和打印文件
// {fullscreen: true, ...}
reader.view(options);

// 后台打印，可用于打印套餐
// print_settings eg:
// "1-3,5,10-8,odd,fit,bin=2" prints pages 1, 3, 5, 9 (i.e. the odd pages from the ranges 1-3, 5-5 and 10-8) and scales them so that they fit into the printable area of the paper.
// "3x" : prints the document 3 times.
reader.silent_print({pdf: 'path', print_settings: '1-10', ...});

// 显示打印对话框，由用户决定怎么打印
// print_settings是否有效未测试
reader.dialog_print({pdf: 'path', print_settings: 'odd', ...});

```

### Ref
- https://blog.risingstack.com/node-hero-node-js-unit-testing-tutorial/
- https://nodejs.org/api/os.html#os_os_platform
- https://blog.risingstack.com/node-js-project-structure-tutorial-node-js-at-scale/
- https://devcenter.heroku.com/articles/node-best-practices#stick-with-lowercase
- http://crockford.com/javascript/
- https://shapeshed.com/writing-cross-platform-node/
- https://www.sumatrapdfreader.org/docs/Command-line-arguments.html
