#!/bin/sh
TAG="tags"
if [ -f $TAG ]; then
    rm $TAG
fi
find ./ -name "*.js" -not -path "./node_modules/*" | xargs ctags --append --fields=+aimS --languages=JavaScript -f $TAG
