var _=require('lodash');

var config = {
    global_group: {
        worker: {
            default: './bin/SumatraPDF.exe'
        },
        demo: {
            default: './demo/Demo.pdf'
        },
        format: {
            default: 'pdf',
            options: ['pdf', 'epub', 'chm']
        },
        file: {  // input file
            default: ''
        },
        platform: {
            default: 'win32',
            options: ['win32']
        }
    },

    //  options for print mode
    print_group: {

        silent_group: {
            print_to: {
                type: 'argument',
                default: 'default',
                cmd_string: '-print-to'
            },
            print_settings: {
                type: 'argument',
                default: '',
                cmd_string: '-print-settings'
            },
            silent: {
                type: 'switch',
                default: true,
                cmd_string: '-silent'
            },
            print_to_default: {
                type: 'switch',
                default: true,
                cmd_string: '-print-to-default'
            },
        },
        dialog_group: {
            print_dialog: {
                type: 'switch',
                default: false,
                cmd_string:'-print-dialog'
            },
            exit_when_done: {
                type: 'switch',
                default: false,
                cmd_string: '-exit-when-done'
            },
        }

    },

    //  options for view mode
    view_group: {

        restrict: {
            type: 'switch',
            default: true,
            cmd_string: '-restrict',
        },

        fullscreen: {
            type: 'switch',
            default: false,
            cmd_string: '-fullscreen'
        },

        presentation: {
            type: 'switch',
            default: false,
            cmd_string: '-presentation'
        },

        zoom: {
            type: 'argument',
            default: 'fit page',
            cmd_string: '-zoom',
            options:[
                "fit page",
                "fit width",
                "fit content"
            ]
        },

        view_mode:{
            type: 'argument',
            default: "single page",
            cmd_string: '-view',
            options: [
                "single page",
                "continuous single page",
                "facing",
                "continuous facing",
                "book view",
                "continuous book view"
            ]
        },

        page_no: {
            type: 'argument',
            default: 1,
            cmd_string: '-page'
        },

        scroll: {
            type: 'argument',
            default: '0,0',
            cmd_string: '-scroll'
        }
    },

    get_defaults: function(){
        return _get_defaults(this);
    },

    valid_option: function(option, value){
        var key;
        var result = false;

        key = _get_by_field(this, option);
        console.log('get key: ' + key);
        if (!key) {
            throw "no key";
        }
        if (key && key['options']) {
            if (_.indexOf(key['options'], value) >= 0) {
                result = true;
            }
        }

        return result;
    },

    get_group_by_name: function(gname, config){
        var gname = gname + '_group';
        var config = config || this;
        var group = null;

        for (var own in config) {
            if (own === gname && typeof(config[own]) == 'object') {
                group = config[own];
                break;
            }
        }
        return group;
    }
};

function _get_by_field(config, field_name){
    var value = false;
    if (field_name in config) {
        value = config[field_name];
    } else {
        for (var i in config) {
            if (!value && typeof(config[i]) == 'object') {
                value = _get_by_field(config[i], field_name);
            }
        }
    }

    return value;
}

function _get_defaults(config){
    var defaults = {}
    _.forInRight(config, function(grp, k){
        if (!(typeof(grp) == 'function')) {
            var ds = _.transform(grp, function(acc, v, k){
                if (!_.endsWith(k, '_group')) {
                    if (v['default'] || v['default'] === false || v['default'] === '') {
                        acc[k] = v['default']
                    }
                } else {
                    acc = _.merge(acc, _get_defaults({k:v}));
                }

                return acc;
            }, {});
            _.merge(defaults, ds);
        }
    });

    return defaults;
}

function _search(hay, needle, accumulator) {
    var accumulator = accumulator || [];
    if (typeof hay == 'object') {
        for (var i in hay) {
            _search(hay[i], needle, accumulator) == true ? accumulator.push(hay) : 1;
        }
        return needle in hay || accumulator;
    }
    //return new RegExp(needle).test(hay) || accumulator;
}

module.exports = config;
