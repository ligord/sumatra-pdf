var os = require("os");
var exec = require("child_process").exec;
var spawn = require("child_process").spawn;

var _ = require("lodash");
var config = require("./config");

exports = module.exports = Sumatra;
function Sumatra(options) {
  var options = options || {};
  var self = this;

  var platform = os.platform();
  if (!config.valid_option("platform", platform)) {
    throw new Error("Unsupported platform: " + platform);
  }

  options = _filte_non_option(options);
  this.options = _.defaults(options, config.get_defaults());

  // private
  function _filte_non_option(options) {
    _.forEach(["view_mode", "zoom"], function (field_name) {
      if (
        options.field_name &&
        !config.valid_option(field_name, options[field_name])
      ) {
        delete options[field_name];
      }
    });
    return options;
  }

  function _exec(cmd_options) {
    if (!self.options.file) {
      throw new Error("No file input!");
    }
    // specify file
    cmd_options.push(self.options.file);
    // console.log("cmd options: " + cmd_options);
    spawn(__dirname + "/../" + self.options.worker, cmd_options);
  }

  //real print;
  function _print(options) {
    var options = options || this.options;
    var cmd_options = [];

    var cfg = options.silent
      ? config.get_group_by_name("silent", config.get_group_by_name("print"))
      : config.get_group_by_name("dialog", config.get_group_by_name("print"));
    cmd_options = _option_to_cmd(cfg, options);
    //console.log("global options: " + _.toPairs(self.options));
    _exec(cmd_options);

    return 0;
  }

  // open service api
  this.view = function (options) {
    var options = _.defaults(
      _.defaults(_filte_non_option(options), config.get_defaults()),
      this.options
    );
    var vcfg = config.get_group_by_name("view");
    var cmd_options = _option_to_cmd(vcfg, options);
    // console.log(cmd_options);
    _exec(cmd_options);

    return 0;
  };

  this.silent_print = function (options) {
    var print_options = {
      silent: true,
      print_dialog: false,
    };
    var options = _.defaults(_.defaults(print_options, options), this.options);
    // console.log(options);

    return _print(options);
  };

  this.dialog_print = function (options) {
    var print_options = {
      silent: false,
      print_dialog: true,
      print_settings: "",
    };
    options = _.defaults(_.defaults(print_options, options), this.options);

    return _print(options);
  };
}

/*
 * config: the config or sub-object of the config
 * options: merged passin options
 */
function _option_to_cmd(config, options) {
  var cmd_options = [];

  _.forOwn(config, function (v, k) {
    if (v["type"] == "switch" && options[k] == true) {
      cmd_options.push(v["cmd_string"]);
    } else if (v["type"] == "argument") {
      cmd_options.push(v["cmd_string"]);
      cmd_options.push(options[k]);
    }
  });
  return cmd_options;
}
