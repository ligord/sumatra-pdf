var assert = require('assert');
var _=require('lodash');

var config = require('../lib/config');

var defaults = {
    worker: './bin/SumatraPDF.exe',
    demo: './demo/Demo.pdf',
    format: 'pdf',
    files: '',
    platform: 'win32',

    //  options for print mode
    print_to: "default", // or print-name
    print_to_default: true,
    print_settings: "", // page range, even or odd, times
    silent: true,
    print_dialog: false,
    exit_when_done: false,

    //  options for view mode
    restrict: true,
    fullscreen: false,
    presentation: false,
    zoom: "fit page",
    page_no: 1, // scroll to the page number
    scroll: "0,0", // scroll to the given coordinates
    view_mode: "single page"
};

describe('Compare default config', function() {
    it('get default', function(){
        assert.deepStrictEqual(
            config.get_defaults(),
            defaults
        );
    });
});

describe('Valid options', function() {
    it('correct option', function(){
        assert.ok(config.valid_option('zoom', 'fit content'), 'options');
    });
    it('correct option', function(){
        assert.ok(config.valid_option('view_mode', 'continuous facing'), 'options');
    });
});

describe('Not exist key', function() {
    it('Not exist key', function(){
        () => {
            config.valid_option('plat-form', 'win32'), 'options';
        },
            Error
    });
});

describe('Get Group', function() {
    it('By name ', function(){
        var g = config.get_group_by_name('print');
        assert.ok(_.has(g, ['silent_group', 'print_settings']), 'silent_group print_settings');
    });
});
