var assert = require('assert');
var Sumatra = require('..')

var demo = './demo/wuxun.pdf'
var pdf = new Sumatra({
    'file': demo, 'page_no': 65, 'presentation': true, 'restrict': false,
    'zoom': 'fit width', 'view_mode': 'continuous book view'
})

/*
describe('Preview', function() {
    it('Presentation mode, to page 100', function(){
        pdf.view({'page_no': 10});
    });
});

describe('Open dialog', function() {
    it('Should open dialog', function() {
        pdf.dialog_print({pdf: demo});
    });
});

describe('No pdf', function() {
    it('Should throw exception', function() {
        assert.throws(
            pdf.print_dialog(),
            new Error("No pdf")
        )
        done()
    });
});

*/
describe('Silent', function() {
    it('Print backgroud', function() {
        pdf.silent_print({
            files: demo,
            print_settings: '1-3, 5, 10-8, odd, fit, bin=2',
            print_to: 'Foxit Reader PDF Printer'
        });
    });
});
